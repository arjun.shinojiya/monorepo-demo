# RegisterDemo

This project was generated using [Nx](https://nx.dev).

# Project Features 


- Project Created with Nx monorepo
- Added seperate common-services library 
- Created and added validations in registration form with Reactive forms

# How to start Project


- Clone the repository
- Run command `npm install`
- After that run command `npm start`
- Project will start on **4200** port
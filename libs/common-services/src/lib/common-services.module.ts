
import { UtilService } from '../lib/util.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToasterModule } from 'angular2-toaster';

@NgModule({

  imports: [
    CommonModule,
  ],
  exports: []
})
export class CommonServicesModule {}

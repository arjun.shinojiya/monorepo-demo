import { FormdetailsComponent } from './../formdetails/formdetails.component';
import { UtilService } from '@register-demo/common-services';

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// Added seperate validators file for setting validations
import { passwordValidator, matchingPasswords, emailValidator, mobileNumberValidator } from '../app-validators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'register-demo-registerform',
  templateUrl: './registerform.component.html',
  styleUrls: ['./registerform.component.css']
})
export class RegisterformComponent implements OnInit {

  public registerForm: FormGroup;
  modelReference: any;
  public mask = [/[1-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
  constructor(
    public fb: FormBuilder,
    public utilService: UtilService,
    private modelService: NgbModal,
  ) { }

  ngOnInit(): void {

    // Form validations
    this.registerForm = this.fb.group({
      userName: ['',  Validators.compose([Validators.required, Validators.minLength(2), Validators.pattern('^[a-zA-Z \-\']+')])],
      email: ['', Validators.compose([Validators.required, emailValidator])],
      phone: ['', Validators.compose([Validators.required, mobileNumberValidator, Validators.minLength(10)])],
      password: ['', Validators.compose([Validators.required, passwordValidator])],
      confirmPassword: ['', Validators.compose([Validators.required])],
      isCheckTerms: [false]
    },{validator: matchingPasswords('password', 'confirmPassword')});
  }

  // On submit form
  submitForm() {
    if (this.registerForm.valid) {
      console.log('valid', this.registerForm.valid)
      this.utilService.showSuccess('Success', 'Your form has been submitted');
       // Open FormDetails
       this.modelReference = this.modelService.open(FormdetailsComponent, {backdrop: false});
       this.modelReference.componentInstance.formValues = this.registerForm.value;
       this.modelReference.result.then(res => {
          this.registerForm.reset()
      });
    }
  }

}

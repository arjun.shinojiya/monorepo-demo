import { FormGroup, FormControl } from '@angular/forms';
import { invalid } from '@angular/compiler/src/render3/view/util';

export function emailValidator(control: FormControl): {[key: string]: any} {
    const emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    if (control.value && !emailRegexp.test(control.value)) {
        return {invalidEmail: true};
    }
}

export function passwordValidator(control: FormControl): {[key: string]: any} {
    const passwordRegexp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]+$/;
    if (control.value && !passwordRegexp.test(control.value)) {
        return { invalidPassword: true };
    }
}

export function matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
        const password = group.controls[passwordKey];
        const passwordConfirmation = group.controls[passwordConfirmationKey];
        if (password.value !== passwordConfirmation.value) {
            return passwordConfirmation.setErrors({mismatchedPasswords: true});
        }
    };
}

export function mobileNumberValidator(control: FormControl): {[key: string]: any} {
    const mobileRegExp = /^(?:\+)?((1)[\s\-])?((\([0-9]{3}\))|[0-9]{3})[\s\-]?[\0-9]{3}[\s\-]?[0-9]{4}$/;
    if ( control.value && !mobileRegExp.test(control.value) ) {
        return { invalidMobileNumber: true };
    }
}

import { Component } from '@angular/core';
import { ToasterConfig, BodyOutputType } from 'angular2-toaster';

@Component({
  selector: 'register-demo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'register-demo';
  public config: ToasterConfig = new ToasterConfig({
    limit: 1,
    preventDuplicates: true,
    bodyOutputType: BodyOutputType.TrustedHtml
  });
}

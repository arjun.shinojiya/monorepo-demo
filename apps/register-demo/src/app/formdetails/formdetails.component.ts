import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'register-demo-formdetails',
  templateUrl: './formdetails.component.html',
  styleUrls: ['./formdetails.component.css']
})
export class FormdetailsComponent implements OnInit {

  @Input() formValues;
  constructor(
    private activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {

  }

  // On close modal
  closeModal() {
    this.activeModal.close()
  }

}

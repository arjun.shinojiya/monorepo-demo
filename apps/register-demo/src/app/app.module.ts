// Toaster plugin for angular
import { ToasterModule } from 'angular2-toaster';
import { CommonServicesModule } from '@register-demo/common-services';
// Angular masking plugin
import { TextMaskModule } from "angular2-text-mask";
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { RegisterformComponent } from './registerform/registerform.component';
import { FormdetailsComponent } from './formdetails/formdetails.component';
// Ng bootstrap for open modal
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [AppComponent, RegisterformComponent, FormdetailsComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CommonServicesModule,
    ToasterModule.forRoot(),
    TextMaskModule,
    BrowserAnimationsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [FormdetailsComponent]
})
export class AppModule {}

import { RegisterformComponent } from './registerform/registerform.component';
import { Routes, RouterModule, ExtraOptions } from "@angular/router";
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: "register",
    component: RegisterformComponent
  },
  {
    path: "",
    redirectTo: "register",
    pathMatch: "full"
  },
  { path: "**", redirectTo: "register" }
]

const config: ExtraOptions = {
  useHash: false,
  enableTracing: false
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

